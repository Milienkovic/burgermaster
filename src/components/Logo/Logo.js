import React from 'react';
import Logo from '../../assets/images/133 burger-logo.png';
import styles from './Logo.module.css';

const logo =(props)=>(
    <div className={styles.Logo}>
        <img src={Logo} alt='Logo'/>
    </div>
);

export default logo;