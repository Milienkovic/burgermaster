import axios from 'axios';

const instance = axios.create({
    baseURL:'https://react-burgermaster.firebaseio.com/'
});

export default instance;