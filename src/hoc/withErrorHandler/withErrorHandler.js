import React, {Component} from 'react';
import Modal from '../../components/UI/Modal/Modal';
import Aux from '../Auxilary';

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        state = {
            error: null
        }


        componentWillMount() {
            this.reqInterceptor = axios.interceptors.request.use(value => {
                this.setState({error: null});
                return value;
            });

            this.responseInterceptor=axios.interceptors.response.use(res => res, e => {
                this.setState({error: e});
            });
        }

        componentWillUnmount() {
            console.log("inside will unmount", this.reqInterceptor, this.responseInterceptor);
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.responseInterceptor);
        }

        errorConfirmedHandler = () => {
            this.setState({error: null})
        }

        render() {
            return (
                <Aux>
                    <Modal show={this.state.error}
                           modalClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent {...this.props}/>
                </Aux>
            );
        }
    }
}

export default withErrorHandler;